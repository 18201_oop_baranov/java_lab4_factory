package ru.nsu.g.ibaranov.factory.factory.car;

public class Auto extends UniqueEntity{

    private final Body body;
    private final Motor motor;
    private final Accessory accessory;

    public Auto(Body body, Motor motor, Accessory accessory)
    {
        this.body = body;
        this.motor = motor;
        this.accessory = accessory;
    }

    @Override
    public String toString()
    {
        return String.format("Auto %d (%s, %s, %s)", this.getId(), this.body, this.motor, this.accessory);
    }
}
