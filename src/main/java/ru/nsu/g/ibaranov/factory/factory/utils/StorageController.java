package ru.nsu.g.ibaranov.factory.factory.utils;

import ru.nsu.g.ibaranov.factory.factory.car.Auto;
import ru.nsu.g.ibaranov.factory.factory.car.UniqueEntity;
import ru.nsu.g.ibaranov.factory.factory.model.Factory;
import ru.nsu.g.ibaranov.factory.factory.view.Updater;
import ru.nsu.g.ibaranov.factory.threadpool.AutoTask;

import java.util.logging.Level;
import java.util.logging.Logger;

public class StorageController extends UniqueEntity {

    private static final Logger logger = FactoryLogger.getLogger();

    private final Factory factory;
    private int autosDispatched = 0;
    private Updater factoryWindow;
    //private final ThreadPool threadPool;

    public StorageController(Factory factory)
    {
        this.factory = factory;
        //this.threadPool = new ThreadPool(autoTasks.size());
    }

    public Auto askForNewAuto() throws InterruptedException {
        logger.log(Level.INFO, "{0} got a request for a new auto", Thread.currentThread().getName());

        synchronized (this.factory.autoStorage)
        {
            if (this.factory.autoStorage.getCurrentSize() < 3) {
                logger.log(Level.INFO, "{0} send 2 tasks for making a new auto ({1} tasks in a queue)",
                        new Object[]{this, this.factory.threadPool.getNumberOfTasks()});
                for (int i = 0; i < 2; i++) {
                    var autoTask = new AutoTask(this.factory.bodyStorage,
                            this.factory.motorStorage,
                            this.factory.accessoryStorage,
                            this.factory.autoStorage);
                    this.factory.threadPool.execute(autoTask);

                }
            }


            while (this.factory.autoStorage.isEmpty()) {
                logger.log(Level.INFO, "{0} waits for a new auto", Thread.currentThread().getName());
                this.factory.autoStorage.wait();
            }

            var newAuto = this.factory.autoStorage.pop();
            this.factory.autoStorage.notify();


            this.autosDispatched += 1;
               // this.factory.logger.log(Level.INFO, this.getClass().getName() + " bought an auto");

            Updater.updateIfNotNull(this.factoryWindow);
//            if (this.view != null) {
//                this.view.update();
//            }

            return newAuto;
        }
    }

    public int getAutosDispatched()
    {
        return autosDispatched;
    }

    public void setFactoryWindow(Updater factoryWindow)
    {
        this.factoryWindow = factoryWindow;
    }
}
