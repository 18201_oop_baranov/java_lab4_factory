package ru.nsu.g.ibaranov.factory.factory.suplier;

import ru.nsu.g.ibaranov.factory.factory.car.UniqueEntity;
import ru.nsu.g.ibaranov.factory.factory.utils.FactoryLogger;
import ru.nsu.g.ibaranov.factory.factory.utils.Storage;

import java.util.function.Supplier;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DetailSupplier<T extends UniqueEntity> extends UniqueEntity implements Runnable{

    private final Storage<T> storage;
    private final Supplier<T> supplier;
    private long delay;
    private final Logger logger = FactoryLogger.getLogger();

    public DetailSupplier(Storage<T> storage, Supplier<T> supplier, int delay)
    {
        this.storage = storage;
        this.supplier = supplier;
        this.delay = delay;
    }

    public void setDelay(int delay)
    {
        this.delay = delay;
    }

    @Override
    public void run()
    {
        while (true)
        {
            var newEntity = this.supplier.get();
            synchronized (this.storage)
            {
                while (this.storage.isFull())
                {
                    try
                    {
                        logger.log(Level.INFO, "{0} is waiting for a {1} to be available", new Object[] {this,this.storage});
                        this.storage.wait();
                    }
                    catch (InterruptedException e)
                    {
                        e.printStackTrace();
                        logger.log(Level.WARNING, "{0} was interrupted", this);
                    }
                }
                this.storage.push(newEntity);
                logger.log(Level.INFO, "{0} pushed {1} to {2}", new Object[] {this,newEntity,this.storage});
            }
            try {
                Thread.sleep(delay);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
