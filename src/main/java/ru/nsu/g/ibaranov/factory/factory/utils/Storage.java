package ru.nsu.g.ibaranov.factory.factory.utils;

import ru.nsu.g.ibaranov.factory.factory.car.UniqueEntity;
import ru.nsu.g.ibaranov.factory.factory.view.Updater;

import java.util.Stack;

public class Storage<T extends UniqueEntity> extends UniqueEntity {

    private Updater factoryWindow;
    public int space;
    private Stack<T> entities = new Stack<>();

    public Storage(int space)
    {
        this.space = space;
    }

    public void push(T entity)
    {
        if (this.entities.size() < this.space) {
                this.entities.push(entity);
                Updater.updateIfNotNull(this.factoryWindow);
                this.notify();
        }
    }

    public T pop()
    {
        if (!this.isEmpty()) {
            var ent = this.entities.pop();
            Updater.updateIfNotNull(this.factoryWindow);
            this.notify();
            return ent;
        }
        else {
            Updater.updateIfNotNull(this.factoryWindow);
            return null;
        }
    }

    public int getCurrentSize()
    {
        if (this.entities.size() != 0) {
            return this.entities.size();
        }
        return 0;
    }

    public int getSpace()
    {
        return space;
    }

    public synchronized boolean isFull()
    {
        if (this.entities.size() == this.space)
        {
            return true;
        }
        return false;
    }

    public  synchronized  boolean isEmpty()
    {
        return this.entities.isEmpty();
    }

    public void setFactoryWindow(Updater factoryWindow)
    {
        this.factoryWindow = factoryWindow;
    }
}
