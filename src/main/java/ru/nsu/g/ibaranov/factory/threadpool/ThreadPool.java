package ru.nsu.g.ibaranov.factory.threadpool;

import ru.nsu.g.ibaranov.factory.factory.view.Updater;

import java.util.ArrayList;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.Executor;

public class ThreadPool implements Runnable, Executor {

    private final Queue<Runnable> tasks = new ConcurrentLinkedQueue<>();
    private ArrayList<Worker> threads;

    private Updater factoryWindow;

    public boolean isRunning = false;

    public ThreadPool(int count) {
        if (count < 1) {
            System.out.println("the number of thread have to be > 0");
            return;
        }

        this.threads = new ArrayList<>(count);
        for (int i = 0; i < count; i++) {
            this.threads.add(new Worker());
        }
    }

    public int getNumberOfWorkers() {
        return this.threads.size();
    }

    public String getStatus(int numberOfWorker) {
        if (numberOfWorker < this.getNumberOfWorkers()) return this.threads.get(numberOfWorker).getStatus();
        return "Unknown";
    }

    @Override
    public void run() {
        for (var thread : this.threads) {
            thread.start();
        }
    }

    public void stop() throws InterruptedException {
        for (var thread : this.threads) {
            thread.interrupt();
        }

        for (var thread : this.threads)
        {
            thread.join();
        }

    }

    public int getNumberOfTasks() {
        return this.tasks.size();
    }

    public void setFactoryWindow(Updater factoryWindow) {
        this.factoryWindow = factoryWindow;
    }

    @Override
    public void execute(Runnable task) {
        synchronized (this.tasks) {
            this.tasks.add(task);
            this.tasks.notify();
        }
        //  synchronized (this.factoryWindow) {
        //  Updater.updateIfNotNull(this.factoryWindow);
        // }
    }

    private class Worker extends Thread {

        public String status = "Sleeping";

        @Override
        public void run() {
            Runnable t;
            outer:
            while (true) {
                synchronized (ThreadPool.this.tasks) {
                    while (ThreadPool.this.tasks.isEmpty()) {
                        try {
                            this.status = "Waiting";
                            ThreadPool.this.tasks.wait();
                        } catch (InterruptedException e) {
                            break outer;
                        }
                    }
                    t = tasks.poll();
                }
                    this.status = "Working";
                try {
                    t.run();
                }
                catch (IllegalMonitorStateException e)
                {
                    break;
                }
                this.status = "Finished";
                    Updater.updateIfNotNull(ThreadPool.this.factoryWindow);
            }
        }

        public String getStatus() {
            if (this.status!=null) return this.status;
            return "Unknown";
        }
    }
}
