package ru.nsu.g.ibaranov.factory.factory.view;

public interface Updater {
    void update();

    static void updateIfNotNull(Updater updater) {
        if (updater != null) updater.update();
    }
}
