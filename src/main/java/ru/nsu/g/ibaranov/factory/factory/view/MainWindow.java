package ru.nsu.g.ibaranov.factory.factory.view;
import ru.nsu.g.ibaranov.factory.factory.model.Factory;
import ru.nsu.g.ibaranov.factory.factory.utils.FactoryLogger;

import javax.swing.*;
import javax.swing.border.LineBorder;
import javax.swing.plaf.metal.MetalScrollBarUI;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class MainWindow extends JFrame implements Runnable{

    private int WIDTH = 600;
    private int HEIGHT = 600;
    private Factory factory;

    public MainWindow() throws IOException {
        super("Factory");
        //this.setIconImage(getIcon());
        Factory.FactoryProperties properties = new Factory.FactoryProperties();
        this.factory = new Factory();
        this.factory.properties = properties;
        this.setSize(WIDTH, HEIGHT);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setResizable(false);
        this.setLocationRelativeTo(null);

        setFocusable(true);

        this.showMenu(this.getContentPane());
    }

    public void showMenu(Container pane)
    {
        pane.removeAll();
        pane.add(new MenuPanel());
        this.revalidate();
    }

    public void showSettings(Container pane)
    {
        pane.removeAll();
        pane.add(new SettingsPanel());
        this.revalidate();
    }

    public void showFactory(Container pane)
    {
        pane.removeAll();
        pane.add(new FactoryWindow());
        this.revalidate();
    }

    @Override
    public void run() {
        SwingUtilities.invokeLater(()->this.setVisible(true));
    }

    public class MenuPanel extends JPanel {

        public MenuPanel()
        {
            JLayeredPane lp = new JLayeredPane();

            this.setLayout(new BorderLayout());
            var startButton = new JButton();

            startButton.addActionListener(actionEvent -> {
                MainWindow.this.factory.run();
                MainWindow.this.showFactory(MainWindow.this.getContentPane());
            });

            startButton.setFocusPainted(false);
            startButton.setFont(new Font(null, Font.BOLD, 25));
            startButton.setText("Start");
            //startButton.setForeground(Color.black);
            startButton.setBounds(185, 300, 240, 75);
            startButton.setBackground(new Color(81,128,128));
            startButton.setBorder(new LineBorder(Color.black));
            lp.add(startButton, JLayeredPane.PALETTE_LAYER);

            var settingsButton = new JButton();
            settingsButton.setFocusPainted(false);
            settingsButton.setFont(new Font(null, Font.BOLD, 25));
            settingsButton.setText("Settings");
            settingsButton.setBounds(185, 380, 240, 75);
            //settingsButton.setForeground(Color.black);
            settingsButton.setBackground(new Color(81,128,128));
            settingsButton.setBorder(new LineBorder(Color.black));
            lp.add(settingsButton, JLayeredPane.PALETTE_LAYER);

            settingsButton.addActionListener(actionEvent -> MainWindow.this.showSettings(MainWindow.this.getContentPane()));

            MenuBackground bc = new MenuBackground();

            bc.setBounds(0,0,600,600);
            lp.add(bc, JLayeredPane.DEFAULT_LAYER);

            JLabel me = new JLabel();
            me.setText("NSU Baranov I.N. 18201");
            me.setFont(new Font(null, Font.PLAIN, 10));
            me.setBounds(240, 500, 180, 40);
            me.setForeground(Color.ORANGE);
            me.setVisible(true);
            lp.add(me, JLayeredPane.POPUP_LAYER);

            this.add(lp);
        }
    }

    public class FactoryWindow extends JPanel implements Runnable, Updater {

        private final JLabel bodiesCount = new JLabel();
        private final JLabel motorsCount = new JLabel();
        private final JLabel accessoryCount = new JLabel();
        private final JLabel autosCount = new JLabel();
        private final JLabel autosSoldCount = new JLabel();
        private final JLabel tasksCount = new JLabel();
        private BoundedRangeModel[] models = new BoundedRangeModel[4];
        private final JLabel[] statuses = new JLabel[MainWindow.this.factory.threadPool.getNumberOfWorkers()];

        FactoryWindow()
        {
            MainWindow.this.factory.storageController.setFactoryWindow(this);
            MainWindow.this.factory.motorStorage.setFactoryWindow(this);
            MainWindow.this.factory.bodyStorage.setFactoryWindow(this);
            MainWindow.this.factory.accessoryStorage.setFactoryWindow(this);
            MainWindow.this.factory.autoStorage.setFactoryWindow(this);
            MainWindow.this.factory.threadPool.setFactoryWindow(this);

            this.models[0] = new DefaultBoundedRangeModel(0,0,0,
                        MainWindow.this.factory.bodyStorage.getSpace());
            this.models[1] = new DefaultBoundedRangeModel(0,0,0,
                    MainWindow.this.factory.motorStorage.getSpace());
            this.models[2] = new DefaultBoundedRangeModel(0,0,0,
                    MainWindow.this.factory.accessoryStorage.getSpace());
            this.models[3] = new DefaultBoundedRangeModel(0,0,0,
                    MainWindow.this.factory.autoStorage.getSpace());

            JLayeredPane lp = new JLayeredPane();
            this.setLayout(new BorderLayout());

            JLabel bodies = new JLabel();
            bodies.setText("Bodies");
            bodies.setFont(new Font(null, Font.BOLD, 20));
            bodies.setBounds(25, 25, 180, 40);
            bodies.setForeground(Color.BLACK);
            bodies.setVisible(true);
            lp.add(bodies, JLayeredPane.POPUP_LAYER);

            bodiesCount.setText(String.valueOf(MainWindow.this.factory.bodyStorage.getCurrentSize()));
            bodiesCount.setFont(new Font(null, Font.BOLD, 20));
            bodiesCount.setBounds(150, 25, 180, 40);
            bodiesCount.setForeground(Color.BLACK);
            bodiesCount.setVisible(true);
            lp.add(bodiesCount, JLayeredPane.POPUP_LAYER);

            JLabel motors = new JLabel();
            motors.setText("Motors");
            motors.setFont(new Font(null, Font.BOLD, 20));
            motors.setBounds(25, 55, 180, 40);
            motors.setForeground(Color.BLACK);
            motors.setVisible(true);
            lp.add(motors, JLayeredPane.POPUP_LAYER);

            motorsCount.setText(String.valueOf(MainWindow.this.factory.motorStorage.getCurrentSize()));
            motorsCount.setFont(new Font(null, Font.BOLD, 20));
            motorsCount.setBounds(150, 55, 180, 40);
            motorsCount.setForeground(Color.BLACK);
            motorsCount.setVisible(true);
            lp.add(motorsCount, JLayeredPane.POPUP_LAYER);

            JLabel accessories = new JLabel();
            accessories.setText("Accessories");
            accessories.setFont(new Font(null, Font.BOLD, 20));
            accessories.setBounds(25, 85, 180, 40);
            accessories.setForeground(Color.BLACK);
            accessories.setVisible(true);
            lp.add(accessories, JLayeredPane.POPUP_LAYER);

            accessoryCount.setText(String.valueOf(MainWindow.this.factory.motorStorage.getCurrentSize()));
            accessoryCount.setFont(new Font(null, Font.BOLD, 20));
            accessoryCount.setBounds(150, 85, 180, 40);
            accessoryCount.setForeground(Color.BLACK);
            accessoryCount.setVisible(true);
            lp.add(accessoryCount, JLayeredPane.POPUP_LAYER);

            JLabel autos = new JLabel();
            autos.setText("Autos");
            autos.setFont(new Font(null, Font.BOLD, 20));
            autos.setBounds(25, 115, 180, 40);
            autos.setForeground(Color.BLACK);
            autos.setVisible(true);
            lp.add(autos, JLayeredPane.POPUP_LAYER);

            autosCount.setText(String.valueOf(MainWindow.this.factory.bodyStorage.getCurrentSize()));
            autosCount.setFont(new Font(null, Font.BOLD, 20));
            autosCount.setBounds(150, 115, 180, 40);
            autosCount.setForeground(Color.BLACK);
            autosCount.setVisible(true);
            lp.add(autosCount, JLayeredPane.POPUP_LAYER);

            JLabel autosSold = new JLabel();
            autosSold.setText("AutosSold");
            autosSold.setFont(new Font(null, Font.BOLD, 20));
            autosSold.setBounds(25, 145, 180, 40);
            autosSold.setForeground(Color.BLACK);
            autosSold.setVisible(true);
            lp.add(autosSold, JLayeredPane.POPUP_LAYER);

            autosSoldCount.setText(String.valueOf(MainWindow.this.factory.bodyStorage.getCurrentSize()));
            autosSoldCount.setFont(new Font(null, Font.BOLD, 20));
            autosSoldCount.setBounds(150, 145, 180, 40);
            autosSoldCount.setForeground(Color.BLACK);
            autosSoldCount.setVisible(true);
            lp.add(autosSoldCount, JLayeredPane.POPUP_LAYER);

            JLabel tasks = new JLabel();
            tasks.setText("Tasks");
            tasks.setFont(new Font(null, Font.BOLD, 20));
            tasks.setBounds(25, 175, 180, 40);
            tasks.setForeground(Color.BLACK);
            tasks.setVisible(true);
            lp.add(tasks, JLayeredPane.POPUP_LAYER);

            tasksCount.setText(String.valueOf(MainWindow.this.factory.threadPool.getNumberOfTasks()));
            tasksCount.setFont(new Font(null, Font.BOLD, 20));
            tasksCount.setBounds(150, 175, 180, 40);
            tasksCount.setForeground(Color.BLACK);
            tasksCount.setVisible(true);
            lp.add(tasksCount, JLayeredPane.POPUP_LAYER);

            //lp.add(bc, JLayeredPane.DEFAULT_LAYER);
            for (int i=0; i<4; i++)
            {
                JProgressBar jpb = new JProgressBar(0,100);
                //jpb.setIndeterminate(true);
                jpb.setStringPainted(true);
                //jpb.setFont(new Font(null, Font.BOLD, 25));
                jpb.setForeground(new Color(81,128,128));
                //jpb.setBackground();
                jpb.setModel(this.models[i]);
                //jpb.setValue(MainWindow.this.factory.bodyStorage.getCurrentSize());
                jpb.setBounds(250, 30+i*30, 300, 25);
                lp.add(jpb);
            }

            //statuses = new JLabel[MainWindow.this.factory.threadPool.getNumberOfWorkers()];

            for (int i=0; i<MainWindow.this.factory.threadPool.getNumberOfWorkers(); i++)
            {
                JLabel worker = new JLabel();
                worker.setText("Worker" + " " + (i+1));
                worker.setFont(new Font(null, Font.PLAIN, 20));
                worker.setBounds(25, 205+30*i, 180, 40);
                worker.setForeground(Color.BLACK);
                worker.setVisible(true);
                lp.add(worker, JLayeredPane.POPUP_LAYER);

                statuses[i] = new JLabel();
                statuses[i].setText(MainWindow.this.factory.threadPool.getStatus(i));
                statuses[i].setFont(new Font(null, Font.PLAIN, 20));
                statuses[i].setBounds(150, 205+30*i, 180, 40);
                statuses[i].setForeground(Color.BLACK);
                statuses[i].setVisible(true);
                lp.add(statuses[i], JLayeredPane.POPUP_LAYER);
            }

            JButton exit = new JButton();
            exit.setText("Exit");
            exit.setBounds(255,500, 300,50);
            exit.setFocusPainted(false);
            exit.setFont(new Font(null, Font.BOLD, 25));
            exit.setBackground(new Color(81,128,128));
            exit.setBorder(new LineBorder(Color.black));
            exit.addActionListener(actionEvent ->
            {
                MainWindow.this.showMenu(MainWindow.this.getContentPane());
                try {
                    MainWindow.this.factory.threadPool.stop();
                    try {
                        FileWriter fstream = new FileWriter("factoryLog.txt");
                        BufferedWriter out = new BufferedWriter(fstream);
                        out.write("LOG");
                        out.close();
                    }
                    catch (Exception e)
                    {
                        System.err.println("Error in cleaning a log file" + e.getMessage());
                    }
                    MainWindow.this.factory = new Factory();
                } catch (InterruptedException | IOException e) {
                    e.printStackTrace();
                }
            });
            lp.add(exit);

            FactoryBackground fb = new FactoryBackground();
            fb.setBounds(0,0,600,600);
            lp.add(fb, JLayeredPane.DEFAULT_LAYER);

            this.add(lp);
        }

        @Override
        public void update()
        {
            try {
                String str = MainWindow.this.factory.bodyStorage.getCurrentSize()
                        + "/" + MainWindow.this.factory.bodyStorage.getSpace();
                this.bodiesCount.setText(str);
                this.models[0].setValue(MainWindow.this.factory.bodyStorage.getCurrentSize());
                String str2 = MainWindow.this.factory.motorStorage.getCurrentSize()
                        + "/" + MainWindow.this.factory.motorStorage.getSpace();
                this.motorsCount.setText(str2);
                this.models[1].setValue(MainWindow.this.factory.motorStorage.getCurrentSize());
                String str3 = MainWindow.this.factory.accessoryStorage.getCurrentSize()
                        + "/" + MainWindow.this.factory.accessoryStorage.getSpace();
                this.accessoryCount.setText(str3);
                this.models[2].setValue(MainWindow.this.factory.accessoryStorage.getCurrentSize());
                String str4 = MainWindow.this.factory.autoStorage.getCurrentSize()
                        + "/" + MainWindow.this.factory.autoStorage.getSpace();
                this.autosCount.setText(str4);
                this.models[3].setValue(MainWindow.this.factory.autoStorage.getCurrentSize());
                String str5 = String.valueOf(MainWindow.this.factory.storageController.getAutosDispatched());
                this.autosSoldCount.setText(str5);
                String str6 = String.valueOf(MainWindow.this.factory.threadPool.getNumberOfTasks());
                this.tasksCount.setText(str6);
                for (int i = 0; i < MainWindow.this.factory.threadPool.getNumberOfWorkers(); i++) {
                    String statusString = MainWindow.this.factory.threadPool.getStatus(i);
                    this.statuses[i].setText(statusString);
                }
            }
            catch (Exception e)
            {
                //e.printStackTrace();
            }
        }

        @Override
        public void run() {

        }
    }

    public class SettingsPanel extends JPanel {

        public SettingsPanel()
        {
            JLayeredPane lp = new JLayeredPane();

            this.setLayout(new BorderLayout());
            var backButton = new JButton();
            backButton.setText("Back");
            backButton.setFocusPainted(false);
            backButton.setBounds(200, 500, 200, 60);
            backButton.setFont(new Font(null, Font.BOLD, 25));
            backButton.setBackground(new Color(81,128,128));
            backButton.setBorder(new LineBorder(Color.black));
            backButton.addActionListener(actionEvent -> MainWindow.this.showMenu(MainWindow.this.getContentPane()));
            lp.add(backButton, JLayeredPane.PALETTE_LAYER);


            String[] properties = {"BodyStorageSize " + MainWindow.this.factory.properties.bodyStorageSize,
                    "MotorStorageSize " + MainWindow.this.factory.properties.motorStorageSize,
                    "AccessoryStorageSize " + MainWindow.this.factory.properties.accessoryStorageSize,
                    "AutoStorageSize " + MainWindow.this.factory.properties.autoStorageSize,
                    "AccessorySuppliersCount " + MainWindow.this.factory.properties.accessorySuppliersCount,
                    "WorkersCount " + MainWindow.this.factory.properties.workersCount,
                    "DealersCount "+ MainWindow.this.factory.properties.dealersCount};

            for (int i=0; i<7;i++)
            {
                JLabel property = new JLabel();
                property.setText(properties[i]);
                property.setBounds(30, 120 + i*30, 300, 25);
                property.setFont(new Font(null, Font.BOLD, 16));
                //property.setForeground(Color.BLACK);
                property.setVisible(true);
                lp.add(property, JLayeredPane.PALETTE_LAYER);
            }

            int totalCountOfPeople = MainWindow.this.factory.properties.accessorySuppliersCount +
                                     MainWindow.this.factory.properties.dealersCount + 2;

            Map<String,Integer> People = new HashMap<>(totalCountOfPeople);
            int i=0;
            for (i=0; i<MainWindow.this.factory.properties.accessorySuppliersCount; i++)
            {
                People.put("AccessorySupplier(" + i + ")",1000);
            }

            String[] dealers = new String[MainWindow.this.factory.properties.dealersCount];
            for (int j=i; j<MainWindow.this.factory.properties.dealersCount+i; j++)
            {
                People.put("Dealer(" + (j-i) + ")",1000);
            }
            People.put("bodySupplier",1000);
            People.put("motorSupplier",1000);

            JSlider slider = new JSlider();
            slider.setBounds(350, 290, 200, 50);
            slider.setMinimum(100);
            slider.setMaximum(3700);
            slider.setValue(1000);
            slider.setMajorTickSpacing(900);
            slider.setPaintLabels(true);
            slider.setBackground(new Color(81,128,128));
            lp.add(slider, JLayeredPane.PALETTE_LAYER);

            String[] names = new String[totalCountOfPeople];
            for (int k=0;k<totalCountOfPeople;k++)
            {
                names[k] = People.keySet().toArray()[k].toString();
            }

            JList<String> settingsList = new JList<String>(names);
            settingsList.setBackground(new Color(81,128,128));
            settingsList.addMouseListener(new MouseListener() {
                @Override
                public void mouseClicked(MouseEvent mouseEvent) {
                    slider.setValue(People.get(settingsList.getSelectedValue()));
                }

                @Override
                public void mousePressed(MouseEvent mouseEvent) {
                }

                @Override
                public void mouseReleased(MouseEvent mouseEvent) {
                }

                @Override
                public void mouseEntered(MouseEvent mouseEvent) {
                }

                @Override
                public void mouseExited(MouseEvent mouseEvent) {
                }
            });
            JScrollPane scrollPane = new JScrollPane(settingsList);
            scrollPane.setBounds(350,120,200,160);
            scrollPane.setBackground(new Color(81,128,128));
            scrollPane.getVerticalScrollBar().setUI(new MetalScrollBarUI(){
                @Override
                protected void paintThumb(Graphics g, JComponent c, Rectangle tb)
                {
                    g.setColor(new Color(51,51,51));
                    if (scrollbar.getOrientation() == JScrollBar.VERTICAL)
                    {
                        g.fillRect(tb.x,tb.y,tb.width,tb.height);
                    }
                }

                @Override
                protected void paintTrack(Graphics g, JComponent c, Rectangle tb)
                {
                    g.setColor(new Color(98,114,119));
                    g.fillRect(tb.x, tb.y, tb.width, tb.height);
                }
            });
            lp.add(scrollPane, JLayeredPane.PALETTE_LAYER);

            JButton accept = new JButton();
            accept.setText("accept");
            accept.setFocusPainted(false);
            accept.setBounds(350, 350, 200, 60);
            accept.setFont(new Font(null, Font.BOLD, 25));
            accept.setBackground(new Color(81,128,128));
            accept.setBorder(new LineBorder(Color.black));
            accept.addActionListener(actionEvent ->
            {
                if (settingsList.getSelectedValue() != null) {
                    People.put(settingsList.getSelectedValue(), slider.getValue());
                    if (settingsList.getSelectedValue() == "motorSupplier")
                        MainWindow.this.factory.supplier_crew.setDelay(slider.getValue(), -1);
                    if (settingsList.getSelectedValue() == "bodySupplier")
                        MainWindow.this.factory.supplier_crew.setDelay(slider.getValue(), -2);
                    if (settingsList.getSelectedValue().contains("AccessorySupplier"))
                        MainWindow.this.factory.supplier_crew.setDelay(slider.getValue(),
                                getIndex(settingsList.getSelectedValue()));
                    if (settingsList.getSelectedValue().contains("Dealer"))
                        MainWindow.this.factory.dealer_crew.setDelay(slider.getValue(),
                                getIndex(settingsList.getSelectedValue()));
                }
            });
            lp.add(accept, JLayeredPane.PALETTE_LAYER);

            JButton isLogging = new JButton();
            isLogging.setText("Logging Enabled");
            isLogging.setFocusPainted(false);
            isLogging.setBounds(25, 350, 265, 60);
            isLogging.setFont(new Font(null, Font.BOLD, 25));
            isLogging.setBackground(new Color(81,128,128));
            isLogging.setBorder(new LineBorder(Color.black));
            isLogging.addActionListener(actionEvent -> {
                if (isLogging.getText() == "Logging Enabled") {
                    isLogging.setText("Logging Disabled");
                    FactoryLogger.setLogging(false);
                    try {
                        FileWriter fstream = new FileWriter("factoryLog.txt");
                        BufferedWriter out = new BufferedWriter(fstream);
                        out.write("LOG");
                        out.close();
                    }
                    catch (Exception e)
                    {
                        System.err.println("Error in cleaning a log file" + e.getMessage());
                    }
                }
                else if (isLogging.getText() == "Logging Disabled") {
                    isLogging.setText("Logging Enabled");
                    FactoryLogger.setLogging(true);
                }
            });
            lp.add(isLogging, JLayeredPane.PALETTE_LAYER);

            SettingsBackground bc = new SettingsBackground();
            bc.setBounds(0,0,600,600);
            lp.add(bc, JLayeredPane.DEFAULT_LAYER);

            this.add(lp);
        }

        private int getIndex(String name)
        {
            if (name == "motorSupplier") return -1;
            if (name == "bodySupplier") return -2;
            int bracketInd1 = name.indexOf("(");
            int bracketInd2 = name.indexOf(")");
            String numBr = name.substring(bracketInd1+1,bracketInd2);
            return Integer.parseInt(numBr);
        }
    }
}
