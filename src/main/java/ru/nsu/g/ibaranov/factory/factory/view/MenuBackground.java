package ru.nsu.g.ibaranov.factory.factory.view;

import javax.swing.*;
import java.awt.*;
import java.io.*;

import static javax.imageio.ImageIO.read;

public class MenuBackground extends JPanel {
    Image back;
    public MenuBackground(){
        try {
            back = read(new File("src/Sprites/a.png"));
            //paint(g2d);
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }
    @Override
    public void paint(Graphics g) {
        Graphics2D graphic2d = (Graphics2D) g;
        graphic2d.drawImage(back,0,0,null);
    }
}

