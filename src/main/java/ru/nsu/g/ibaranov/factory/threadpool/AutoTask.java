package ru.nsu.g.ibaranov.factory.threadpool;

import ru.nsu.g.ibaranov.factory.factory.car.*;
import ru.nsu.g.ibaranov.factory.factory.utils.Storage;

public class AutoTask extends UniqueEntity implements Runnable {

    public final Storage<Body> bodyStorage;
    public final Storage<Motor> motorStorage;
    public final Storage<Accessory> accessoryStorage;
    public final Storage<Auto> autoStorage;
    private String workerStatus;

    public AutoTask(Storage<Body> bodyStorage,
                    Storage<Motor> motorStorage,
                    Storage<Accessory> accessoryStorage,
                    Storage<Auto> autoStorage)
    {
        this.bodyStorage = bodyStorage;
        this.motorStorage = motorStorage;
        this.accessoryStorage = accessoryStorage;
        this.autoStorage = autoStorage;
    }

    public String getStatus()
    {
        return this.workerStatus;
    }

    private Body getBody() throws InterruptedException {
        synchronized (this.bodyStorage)
        {
            while (this.bodyStorage.isEmpty())
            {
                this.bodyStorage.wait();
            }

            var newBody = this.bodyStorage.pop();
            this.bodyStorage.notify();
            return newBody;
        }
    }

    private Motor getMotor() throws InterruptedException {
        synchronized (this.motorStorage)
        {
            while (this.motorStorage.isEmpty())
            {
                this.motorStorage.wait();
            }

            var newMotor = this.motorStorage.pop();
            this.motorStorage.notify();
            return newMotor;
        }
    }

    private Accessory getAccessory() throws InterruptedException {
        synchronized (this.accessoryStorage)
        {
            while (this.accessoryStorage.isEmpty())
            {
                this.accessoryStorage.wait();
            }

            var newAccessory = this.accessoryStorage.pop();
            this.accessoryStorage.notify();
            return newAccessory;
        }
    }

    void pushAuto(Auto auto) throws InterruptedException {
        synchronized (this.autoStorage)
        {
            while (this.autoStorage.isFull())
            {
                this.autoStorage.wait();
            }
            this.autoStorage.push(auto);
            this.autoStorage.notify();
        }
    }

    @Override
    public void run()
    {
                try {
                        var body = this.getBody();
                        var motor = this.getMotor();
                        var accessory = this.getAccessory();

                        pushAuto(new Auto(body,motor,accessory));

                } catch (InterruptedException e) {
                    this.bodyStorage.push(new Body());
                    this.motorStorage.push(new Motor());
                    this.accessoryStorage.push(new Accessory());
                    this.autoStorage.push(new Auto(new Body(), new Motor(), new Accessory()));
                }
    }
}
