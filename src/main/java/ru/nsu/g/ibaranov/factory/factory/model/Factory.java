package ru.nsu.g.ibaranov.factory.factory.model;

import ru.nsu.g.ibaranov.factory.factory.car.Accessory;
import ru.nsu.g.ibaranov.factory.factory.car.Auto;
import ru.nsu.g.ibaranov.factory.factory.car.Body;
import ru.nsu.g.ibaranov.factory.factory.car.Motor;
import ru.nsu.g.ibaranov.factory.factory.dealer.Dealer;
import ru.nsu.g.ibaranov.factory.factory.suplier.DetailSupplier;
import ru.nsu.g.ibaranov.factory.factory.utils.FactoryLogger;
import ru.nsu.g.ibaranov.factory.factory.utils.Storage;
import ru.nsu.g.ibaranov.factory.factory.utils.StorageController;
import ru.nsu.g.ibaranov.factory.threadpool.ThreadPool;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Factory implements Runnable{

    public Logger logger;

    public final Storage<Body> bodyStorage;
    public final Storage<Motor> motorStorage;
    public final Storage<Accessory> accessoryStorage;
    public final Storage<Auto> autoStorage;
    public StorageController storageController;
    public ThreadPool threadPool;
    public ArrayList<Thread> threads;

    public final Supplier_crew supplier_crew;
    public final Dealer_crew dealer_crew;

    public FactoryProperties properties = new FactoryProperties();
    //public boolean isLogging = true;

    public Factory() throws IOException {
        //this.properties = new FactoryProperties();

        FactoryLogger.setLogging(true);
        this.logger = FactoryLogger.getLogger();

        //this.logger.log(Level.INFO, "lol");//fine("IM HERE");

        this.bodyStorage = new Storage<>(this.properties.bodyStorageSize);
        this.motorStorage = new Storage<>(this.properties.motorStorageSize);
        this.accessoryStorage = new Storage<>(this.properties.accessoryStorageSize);
        this.autoStorage = new Storage<>(this.properties.autoStorageSize);
        this.supplier_crew = new Supplier_crew(bodyStorage, motorStorage,
                accessoryStorage,
                this.properties.accessorySuppliersCount);

//        this.autoTasks = new ArrayList<>(this.properties.workersCount);
//        for (int i=0; i < this.properties.workersCount; i++)
//        {
//            this.autoTasks.add(new AutoTask(this.bodyStorage,
//                                        this.motorStorage,
//                                       this.accessoryStorage,
//                                         this.autoStorage));
//        }
        this.threadPool = new ThreadPool(this.properties.workersCount);
        this.storageController = new StorageController(this);
        this.dealer_crew = new Dealer_crew(this.storageController, this.properties.dealersCount);
        this.threads = new ArrayList<>(properties.dealersCount + properties.accessorySuppliersCount + 2);
    }

    @Override
    public void run() {
        this.threadPool.run();
        for (var diller : this.dealer_crew.dealers)
        {
            this.threads.add(new Thread(diller));
        }
        for (var accessorySupplier : this.supplier_crew.accessoriesSuppliers)
        {
            this.threads.add(new Thread(accessorySupplier));
        }
        this.threads.add(new Thread(this.supplier_crew.bodySupplier));
        this.threads.add(new Thread(this.supplier_crew.motorSupplier));
        for (var t : this.threads)
        {
            t.start();
        }
    }

    public static final class Supplier_crew {
        private int delay = 1000;

        private final DetailSupplier<Body> bodySupplier;
        private final DetailSupplier<Motor> motorSupplier;
        private final ArrayList<DetailSupplier<Accessory>> accessoriesSuppliers;

        private Supplier_crew(
                Storage<Body> bodyStorage,
                Storage<Motor> motorStorage,
                Storage<Accessory> accessoryStorage,
                int accessorySuppliersCount
                )
        {
            this.bodySupplier = new DetailSupplier<>(bodyStorage, Body::new, delay);
            this.motorSupplier = new DetailSupplier<>(motorStorage, Motor::new, delay);

            this.accessoriesSuppliers = new ArrayList<>(accessorySuppliersCount);

            for (int i=0; i<accessorySuppliersCount; i++)
            {
                this.accessoriesSuppliers.add(new DetailSupplier<>(accessoryStorage, Accessory::new, delay));
            }
        }

        public void setDelay(int delay, int index)
        {
            if (index == -1)
            {
                this.motorSupplier.setDelay(delay);
                return;
            }
            if (index == -2)
            {
                this.bodySupplier.setDelay(delay);
                return;
            }
            this.accessoriesSuppliers.get(index).setDelay(delay);
        }
    }

    public static final class Dealer_crew
    {
        private static final int delay = 1100;

        public final ArrayList<Dealer> dealers;

        private Dealer_crew(StorageController storageController,
                            int dealersCount)
        {
            this.dealers = new ArrayList<>(dealersCount);

            for (int i=0; i<dealersCount; i++)
            {
                this.dealers.add(new Dealer(storageController, delay));
            }
        }

        public void setDelay(int delay, int index)
        {
            this.dealers.get(index).setDelay(delay);
        }
    }

    public static final class FactoryProperties
    {
        public int bodyStorageSize;
        public int motorStorageSize;
        public int accessoryStorageSize;
        public int autoStorageSize;
        public int workersCount;
        public int dealersCount;
        public int accessorySuppliersCount;

        public FactoryProperties() throws IOException {
            String propertiesFile = "/properties";

            var Stream = Factory.class.getResourceAsStream(propertiesFile);

            if (Stream == null) {
                System.out.println("something wrong with configuration file...");
                throw new IOException("failed to create a stream");
            }

            var properties = new Properties();
            properties.load(Stream);

            try {
                this.bodyStorageSize = Integer.parseInt(properties.getProperty("bodyStorageSize"));
                this.motorStorageSize = Integer.parseInt(properties.getProperty("motorStorageSize"));
                this.accessoryStorageSize = Integer.parseInt(properties.getProperty("accessoryStorageSize"));
                this.autoStorageSize = Integer.parseInt(properties.getProperty("autoStorageSize"));
                this.accessorySuppliersCount = Integer.parseInt(properties.getProperty("accessorySuppliersCount"));
                int workersNumber = Integer.parseInt(properties.getProperty("workersCount"));
                if (workersNumber < 11) this.workersCount = Integer.parseInt(properties.getProperty("workersCount"));
                else this.workersCount = 10;
                this.dealersCount = Integer.parseInt(properties.getProperty("dealersCount"));
            }
            catch (NumberFormatException | NullPointerException e)
            {
                System.out.println("failed to load configuration / bad format of data");
            }
        }

    }

}
