package ru.nsu.g.ibaranov.factory;

import ru.nsu.g.ibaranov.factory.factory.car.Body;
import ru.nsu.g.ibaranov.factory.factory.model.Factory;
import ru.nsu.g.ibaranov.factory.factory.view.MainWindow;

import java.io.IOException;

public class Main {
    public static void main(String[] args) throws IOException {

        var MainWindow = new MainWindow();
        MainWindow.run();
    }
}
