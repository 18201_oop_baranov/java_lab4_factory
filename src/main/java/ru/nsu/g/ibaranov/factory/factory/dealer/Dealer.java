package ru.nsu.g.ibaranov.factory.factory.dealer;

import ru.nsu.g.ibaranov.factory.factory.car.UniqueEntity;
import ru.nsu.g.ibaranov.factory.factory.utils.FactoryLogger;
import ru.nsu.g.ibaranov.factory.factory.utils.StorageController;

import java.util.logging.Level;
import java.util.logging.Logger;

public class Dealer extends UniqueEntity implements Runnable {

    private final StorageController storageController;
    private int delay;
    private final Logger logger = FactoryLogger.getLogger();

    public Dealer(StorageController storageController, int delay)
    {
        this.storageController = storageController;
        this.delay = delay;
    }

    public void setDelay(int delay)
    {
        this.delay = delay;
    }

    @Override
    public void run()
    {
        while (true)
        {
            try {
                logger.log(Level.INFO, "{0} asking for an auto form {1}", new Object[] {this, this.storageController});
                var newAuto = this.storageController.askForNewAuto();
                logger.log(Level.INFO, "{0} got {1} from {2}", new Object[] {this, newAuto, this.storageController});
                Thread.sleep(delay);
            } catch (InterruptedException e) {
                e.printStackTrace();
                logger.log(Level.WARNING, "{0} interrupted", this);
                break;
            }
        }
    }

}
