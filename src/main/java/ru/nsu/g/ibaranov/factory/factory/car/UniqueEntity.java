package ru.nsu.g.ibaranov.factory.factory.car;

import java.util.HashMap;

public abstract class UniqueEntity {

    private final int id;
    private static final HashMap<Class<? extends UniqueEntity>, Integer> serialNumbers = new HashMap<>();

    protected UniqueEntity() {
        synchronized (UniqueEntity.serialNumbers) {
            if (!UniqueEntity.serialNumbers.containsKey(this.getClass()))
            {
                UniqueEntity.serialNumbers.put(this.getClass(), 1);
            }

            this.id = UniqueEntity.serialNumbers.get(this.getClass());
            UniqueEntity.serialNumbers.put(this.getClass(), this.id + 1);
        }
    }

    public int getId()
    {
        return this.id;
    }

    @Override
    public String toString()
    {
        return String.format("%s(%d)", this.getClass().getSimpleName(), this.getId()-1);
    }

}
